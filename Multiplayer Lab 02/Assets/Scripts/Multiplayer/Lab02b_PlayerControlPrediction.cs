﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class Lab02b_PlayerControlPrediction : NetworkBehaviour
{
    public enum CharacterState
    {
        Idle = 0,
        WalkingForwards = 1,
        WalkingBackwards = 2,
        Jumping = 4,
        RunningFowards = 3,
        RunningBackwards = 5,
    };

    struct PlayerState
    {
        public int movementNumber;
        public float posX, posY, posZ;
        public float rotX, rotY, rotZ;
        public CharacterState animationState;

    }

    [SyncVar(hook = "OnServerChange")]
    PlayerState serverState;            //It will represent the state of the player on the server

    PlayerState predictedState;         //This will represent the state of the player as predicted
    //Client only
    CharacterState characterAnimationState;
    Queue<KeyCode> pendingMoves;        //This will represent the moves that the player is attempting
    //That have not been acknowledged by the server yet!
    //Remember: Queue is a first-in fist-out list

    public Animator controller;

    public float runSpeed = 5f;

    // Use this for initialization
    void Start()
    {
        InitState();
        predictedState = serverState;

        if (isLocalPlayer)
        {
            pendingMoves = new Queue<KeyCode>();
            UpdatePredictedState();
        }

        SyncState();
    }

    [Server]
    void InitState()
    {
        serverState = new PlayerState
        {
            movementNumber = 0,
            posX = -119f,
            posY = 165.08f,
            posZ = -924f,
            rotX = 0f,
            rotY = 0f,
            rotZ = 0f
        };
    }

    void Update()
    {
        if (isLocalPlayer)
        {
            KeyCode[] possibleKeys = { KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.W, KeyCode.Q, KeyCode.E, KeyCode.Space };
            bool somethingPressed = false;

            foreach (KeyCode possibleKey in possibleKeys)
            {
                if (!Input.GetKey(possibleKey)) //If the currently observed key code is no pressed
                    continue;                   //Then do nothing!

                somethingPressed = true;
                pendingMoves.Enqueue(possibleKey);

                UpdatePredictedState();
                CmdMoveOnServer(possibleKey);

                if (!somethingPressed)
                {
                    pendingMoves.Enqueue(KeyCode.None);
                    UpdatePredictedState();
                    CmdMoveOnServer(KeyCode.None);
                }
            }
            SyncState();
        }
    }

    void SyncState()
    {
        PlayerState statetoRender = isLocalPlayer ? predictedState : serverState;
        transform.position = new Vector3(statetoRender.posX, statetoRender.posY, statetoRender.posZ);
        transform.rotation = Quaternion.Euler(statetoRender.rotX, statetoRender.rotY, statetoRender.rotZ);
        controller.SetInteger("CharacterState", (int)statetoRender.animationState);
    }

    [Command]
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        serverState = Move(serverState, pressedKey);
    }

    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float deltaX = 0, deltaY = 0, deltaZ = 0;
        float deltaRotationY = 0;

        switch (newKey)
        {
            case KeyCode.Q:
                deltaX = -0.5f;
                break;
            case KeyCode.W:
                if (Input.GetKey(KeyCode.R))
                    deltaZ = 0.5f * runSpeed;
                else
                    deltaZ = 0.5f;
                break;
            case KeyCode.E:
                deltaX = 0.5f;
                break;
            case KeyCode.S:
                if (Input.GetKey(KeyCode.R))
                    deltaZ = -0.5f * runSpeed;
                else
                    deltaZ = -0.5f; 
                break;
            case KeyCode.A:
                deltaRotationY = -0.5f;
                break;
            case KeyCode.D:
                deltaRotationY = 0.5f;
                break;
            case KeyCode.Space:
                deltaY = 0.05f;
                break;
        }

        return new PlayerState
        {
            movementNumber = 1 + previous.movementNumber,
            posX = deltaX + previous.posX,
            posY = deltaY + previous.posY,
            posZ = deltaZ + previous.posZ,
            rotX = previous.rotX,
            rotY = deltaRotationY + previous.rotY,
            rotZ = previous.rotZ,
            animationState = CalcAnimation(deltaX, deltaY, deltaZ, deltaRotationY)
        };
    }

    void OnServerChange(PlayerState newState)
    {
        serverState = newState;

        if (pendingMoves != null)
        {
            while (pendingMoves.Count > (predictedState.movementNumber - serverState.movementNumber))
            {
                pendingMoves.Dequeue();
            }

            UpdatePredictedState();
        }
    }

    void UpdatePredictedState()
    {
        predictedState = serverState;

        foreach (KeyCode moveKey in pendingMoves)
        {
            predictedState = Move(predictedState, moveKey);
        }
    }

    CharacterState CalcAnimation(float dX, float dY, float dZ, float dRY)
    {

        if (dX == 0 && dY == 0 && dZ == 0)
            return CharacterState.Idle;

        if (dX != 0 || dZ != 0)
        {
            if (dX > 0 || dZ > 0)
            {
                if (dZ > 0.5f)
                {
                    return CharacterState.RunningFowards;
                }

                else
                    return CharacterState.WalkingForwards;
            }

            else
            {
                if (dZ < -0.5f)
                {
                    return CharacterState.RunningBackwards;
                }

                else
                    return CharacterState.WalkingBackwards;
            }
        }
        return CharacterState.Idle;
    }
}